"""
Fake implementation of a service that exposes an HTTP API
to send emails.

This will actually print the email content on stdout.
"""

from fastapi import FastAPI
from pydantic import BaseModel


app = FastAPI()


class Email(BaseModel):
    addr_from: str
    addr_to: str
    subject: str
    message: str


@app.post("/email")
def email(email: Email):
    print(f"SENDING: {email}")
