""" Test HTTP interface """

import asyncio
import pytest
from pytest_describe import behaves_like
from unittest import mock

from fastapi.testclient import TestClient

from dailyauth import api
from dailyauth import passwords as pwd
from dailyauth.api import app
from dailyauth import exceptions
from dailyauth.types import NewUserInfo, User


@pytest.fixture
def client():
    yield TestClient(app)


@pytest.fixture
def store():
    with mock.patch.object(api, "db", autospec=True) as m:
        yield m


def test_user_registration_id_returned(client, store):
    store.create_user.return_value = "SomeId"

    resp = client.post(
        "/user", json={"email": "toto@glagla.fr", "password": "abcdef"}
    )
    store.create_user.assert_awaited()

    # check call to create_user
    user = store.create_user.await_args[0][0]  # first pos arg
    assert user.email == "toto@glagla.fr"  # using email
    assert pwd.check_password("abcdef", user.password)  # hashing password

    assert resp.status_code == 201
    assert resp.json() == "SomeId"


@pytest.fixture()
def fake_user():
    yield User(
        email="the.user@inside.do", hashed_password="TheHash", is_active=False
    )


def auth_required():
    """ Collection of checks to perform againt different endpoints """

    @pytest.fixture(autouse=True)
    def with_user(store, fake_user):
        store.get_user_by_email.return_value = fake_user

    @pytest.fixture(autouse=True)
    def check_password():
        with mock.patch("dailyauth.api.pwd.check_password") as m:
            m.return_value = True  # password is correct by default
            yield m

    @pytest.fixture()
    def payload():
        return None

    def auth_is_checked(client, check_password, fake_user, endpoint, payload):
        resp = client.request(*endpoint, auth=("user", "passwd"), json=payload)
        # password check is performed against existing hash
        check_password.assert_called_with("passwd", fake_user.hashed_password)
        assert resp.status_code == 200

    def wrong_user_raises(client, store, endpoint, payload):
        store.get_user_by_email.side_effect = exceptions.UserNotFound
        resp = client.request(*endpoint, auth=("user", "pass"), json=payload)
        assert resp.status_code == 401
        assert resp.headers["WWW-Authenticate"] == "Basic"

    def wrong_password_raises(client, check_password, endpoint, payload):
        check_password.return_value = False  # wrong password
        resp = client.request(*endpoint, auth=("user", "pass"), json=payload)
        assert resp.status_code == 401
        assert resp.headers["WWW-Authenticate"] == "Basic"


@behaves_like(auth_required)
def describe_activation_request():
    @pytest.fixture()
    def endpoint():
        return "POST", "/user/request_code"

    @pytest.fixture(autouse=True)
    def send_email():
        with mock.patch("dailyauth.api.send_email") as m:
            yield m

    @pytest.fixture(autouse=True)
    def get_activation_code():
        with mock.patch("dailyauth.api.get_activation_code") as m:
            yield m

    def email_is_sent(client, send_email):
        client.post("/user/request_code", auth=("user", "passwd"))
        send_email.assert_called_once()

    def code_is_saved(client, store, get_activation_code, fake_user):
        get_activation_code.return_value = "0123"
        client.post("/user/request_code", auth=("user", "passwd"))
        store.save_activation_code.assert_awaited_with(fake_user.email, "0123")


@behaves_like(auth_required)
def describe_user_details():
    @pytest.fixture()
    def endpoint():
        return "GET", "/user"

    def correct_details(client, fake_user):
        user = client.get("/user", auth=("user", "Pass"))
        assert user.json() == {
            "email": fake_user.email,
            "is_active": fake_user.is_active,
        }


@behaves_like(auth_required)
def describe_activate_user():
    @pytest.fixture()
    def endpoint():
        return "POST", "/user/activate"

    @pytest.fixture()
    def payload():
        return "0000"

    def activation_performed(client, fake_user, store):
        client.post("/user/activate", auth=("user", "pass"), json="0123")
        store.activate_user.assert_awaited_with(fake_user.email, "0123")

    @pytest.mark.parametrize(
        "error,message",
        [
            (exceptions.IncorrectActivationCode, "incorrect"),
            (exceptions.ActivationRequestNeeded, "request"),
            (exceptions.ActivationCodeExpired, "expired"),
            (exceptions.ActivationError, ""),  # general case should also be HTTP error
        ],
    )
    def activation_error(client, store, error, message):
        """ test different errors and their returned message """
        store.activate_user.side_effect = error
        resp = client.post("/user/activate", auth=("u", "p"), json="XX")
        assert resp.status_code == 403
        assert message in resp.text.lower()
