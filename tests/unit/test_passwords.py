from dailyauth import passwords as pwd


def test_comparison():
    hashed = pwd.get_hashed_password("p455W0rD")
    assert pwd.check_password("p455W0rD", hashed) is True
    assert pwd.check_password("password", hashed) is False
