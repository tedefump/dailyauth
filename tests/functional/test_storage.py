"""
This test module requires a mongodb server running with `user-store` hostname.
"""

import pytest
from freezegun import freeze_time

from bson.objectid import ObjectId

from dailyauth.exceptions import (
    UserAlreadyExists,
    UserNotFound,
    ActivationError,
    ActivationCodeExpired,
    ActivationRequestNeeded,
    IncorrectActivationCode,
)
from dailyauth.passwords import check_password
from dailyauth.storage import UserStore, USER_DB
from dailyauth.types import NewUserInfo

pytestmark = pytest.mark.asyncio


@pytest.fixture()
async def store():
    us = UserStore("mongodb://user-store/")
    await us._client.drop_database(USER_DB)  # always start tests with empty db
    await us.ensure_indexes()
    yield us


async def test_user_creation(store):
    id = await store.create_user(
        # data coming from the FastAPI layer will look like:
        NewUserInfo(email="the@ema.il", password="1234")
    )

    # get the inserted user document, in order to check its content
    inserted = await store._users.find_one({"_id": ObjectId(id)})

    # email is untouched
    assert inserted["email"] == "the@ema.il"

    # user is created inactive
    assert inserted["is_active"] is False

    # password is hashed, and can be verified like so
    assert check_password("1234", inserted["password"])


@pytest.fixture
async def user(store):
    """ An existing user """
    await store.create_user(
        NewUserInfo(email="some@user.com", password="password")
    )
    yield await store.get_user_by_email("some@user.com")


@pytest.fixture
async def activated_user(user, store):
    await store._set_user_active(user.email)
    yield await store.get_user_by_email(user.email)


@pytest.mark.usefixtures("user")
async def test_email_is_unique(store):
    # we want to raise `UserAlreadyExists` when email already exists
    with pytest.raises(UserAlreadyExists):
        await store.create_user(
            NewUserInfo(email="some@user.com", password="w34k")
        )


async def test_find_user(store, user):
    # we can get it back using its email address
    user_ = await store.get_user_by_email("some@user.com")
    assert user.email == user_.email

    # or we get a nice exception when doesn't exist
    with pytest.raises(UserNotFound):
        await store.get_user_by_email("the.other@user.com")


def describe_request_activation_code():
    async def activation_code_is_saved(store, user):
        # new activation request has been asked, code generated
        await store.save_activation_code(user.email, "0608")

        user_ = await store._get_user_document(user.email)
        assert len(user_["activation_codes"]) == 1

        # new requests lead to adding more_codes
        await store.save_activation_code(user.email, "1234")
        await store.save_activation_code(user.email, "5678")

        user_ = await store._get_user_document(user.email)
        assert len(user_["activation_codes"]) == 3

    async def code_not_saved_when_already_activated(store, activated_user):
        await store.save_activation_code(activated_user.email, "XXXX")
        user_ = await store._get_user_document(activated_user.email)
        assert len(user_["activation_codes"]) == 0


def describe_user_activation():
    @pytest.fixture
    async def user_to_activate(user, store):
        """ A user who made an activation request """
        with freeze_time("2010-01-01 12:00:00"):
            await store.save_activation_code(user.email, "0123")
        yield user

    @pytest.fixture
    async def active_user(user_to_activate, store):
        """ A user who has been activated """
        with freeze_time("2010-01-01 12:00:00"):
            await store.activate_user(user_to_activate.email, "0123")
        yield user_to_activate


    @freeze_time("2010-01-01 12:00:15")
    async def correct_code_in_time(user_to_activate, store):
        await store.activate_user(user_to_activate.email, "0123")

        user = await store.get_user_by_email(user_to_activate.email)
        assert user.is_active is True

    @freeze_time("2010-01-01 12:01:10")
    async def is_too_late(user_to_activate, store):
        with pytest.raises(ActivationCodeExpired):
            await store.activate_user(user_to_activate.email, "0123")

        user = await store.get_user_by_email(user_to_activate.email)
        assert user.is_active is False

    @freeze_time("2010-01-01 12:00:10")
    async def is_in_time_but_incorrect(user_to_activate, store):
        with pytest.raises(IncorrectActivationCode):
            await store.activate_user(user_to_activate.email, "0000")

        user = await store.get_user_by_email(user_to_activate.email)
        assert user.is_active is False

    @freeze_time("2010-01-01 12:10:10")
    async def is_late_and_incorrect(user_to_activate, store):
        with pytest.raises(ActivationError):
            await store.activate_user(user_to_activate.email, "0000")

        user = await store.get_user_by_email(user_to_activate.email)
        assert user.is_active is False

    async def no_code_exists(store, user):
        # user hasn't requested a code yet
        with pytest.raises(ActivationRequestNeeded):
            await store.activate_user(user.email, "1234")

    async def user_already_active(store, active_user):
        await store.activate_user(active_user.email, "0123")
        user = await store.get_user_by_email(active_user.email)
        # stays active
        assert user.is_active is True
