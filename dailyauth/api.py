from fastapi import FastAPI, Body, HTTPException, Depends
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from pydantic import BaseModel

from dailyauth import exceptions
from dailyauth import passwords as pwd
from dailyauth.storage import UserStore
from dailyauth.types import NewUserInfo, User, PublicUserDetails
from dailyauth.utils import get_activation_code

from dailyauth.mail_client import send_email

app = FastAPI()

security = HTTPBasic()

db = UserStore("mongodb://user-store")


class BaseResponse(BaseModel):
    detail: str


async def get_user(creds: HTTPBasicCredentials = Depends(security)) -> User:
    """ get user from DB and check credentials """
    try:
        user = await db.get_user_by_email(creds.username)
    except exceptions.UserNotFound:
        user = None

    if user and pwd.check_password(creds.password, user.hashed_password):
        return user

    raise HTTPException(
        401, "Authorization error", headers={"WWW-Authenticate": "Basic"}
    )


@app.post(
    "/user",
    status_code=201,
    responses={
        409: {"description": "User already exists", "model": BaseResponse}
    },
)
async def register_user(details: NewUserInfo):
    try:
        return await db.create_user(details)
    except exceptions.UserAlreadyExists as e:
        raise HTTPException(409, str(e))


@app.post("/user/request_code", response_model=BaseResponse)
async def request_validation_code(
    user: HTTPBasicCredentials = Depends(get_user),
):
    code = get_activation_code()
    send_email(
        user.email,
        "New activation code",
        f"Here's your new code: {code}",
    )
    await db.save_activation_code(user.email, code)
    return BaseResponse(
        detail=f"A code has been sent to {user.email}, it is valid for one minute"
    )


@app.post(
    "/user/activate",
    response_model=BaseResponse,
    responses={
        403: {"description": "Activation error", "model": BaseResponse}
    },
)
async def activate_user(
    user: HTTPBasicCredentials = Depends(get_user),
    code: str = Body(...),
):
    try:
        await db.activate_user(user.email, code)
    except exceptions.ActivationRequestNeeded:
        raise HTTPException(403, "You need to request an activation code.")
    except exceptions.IncorrectActivationCode:
        raise HTTPException(403, "Incorrect activation code.")
    except exceptions.ActivationCodeExpired:
        raise HTTPException(403, "This activation code has expired.")
    except exceptions.ActivationError as e:
        raise HTTPException(403, str(e))
    else:
        return BaseResponse(detail=f"User {user.email} has been activated.")


@app.get("/user", response_model=PublicUserDetails)
async def get_user_details(user: HTTPBasicCredentials = Depends(get_user)):
    return PublicUserDetails(email=user.email, is_active=user.is_active)


@app.on_event("startup")
async def setup_storage():
    await db.ensure_indexes()
