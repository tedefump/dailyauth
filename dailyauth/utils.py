import random


def get_activation_code():
    return "".join(random.choices("0123456789", k=4))
