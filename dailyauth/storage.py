"""
Defines an abstraction on top of the actual DBMS
"""

import datetime
import pymongo
from motor.motor_asyncio import AsyncIOMotorClient

from dailyauth.exceptions import (
    UserAlreadyExists,
    UserNotFound,
    ActivationCodeExpired,
    ActivationRequestNeeded,
    IncorrectActivationCode,
)

from dailyauth.types import NewUserInfo, User

USER_DB = "daily_users"
MAX_ACTIVATION_DELAY = 60


class UserStore:
    """
    Manage Users in the DB. You only need one instance of `UserStore`
    for your entire application, connections will be managed internally.
    """

    def __init__(self, connstring: str):
        # The Motor client manages its own pool of connections to
        # mongoDB, each connection is created on demand, and entirely
        # managed within the client. Therefore, it's safe to have a
        # single instance of such client for the entire lifetime of
        # our app, actual connections will come and go as needed.
        # We'll rely on that feature.
        self._client = AsyncIOMotorClient(connstring)
        self._db = self._client[USER_DB]
        self._users = self._db["users"]

    async def ensure_indexes(self):
        """ Make sure indexes are created """
        await self._users.create_index("email", unique=True)

    async def _get_user_document(self, email):
        user = await self._users.find_one({"email": email})

        if not user:
            raise UserNotFound(email)

        return user

    async def create_user(self, details: NewUserInfo):
        """
        Create a new user in the database.
        The user is created with an "is_active" flag set to False.
        """
        try:
            user_data = details.dict()
            user_data["is_active"] = False
            user_data["activation_codes"] = []
            res = await self._users.insert_one(user_data)
        except pymongo.errors.DuplicateKeyError:
            raise UserAlreadyExists(f"{details.email} already exists")
        return str(res.inserted_id)

    async def get_user_by_email(self, email):
        """
        Retrieve user information based on email
        """
        user = await self._get_user_document(email)

        return User(
            email=user["email"],
            hashed_password=user["password"],
            is_active=user["is_active"],
        )

    async def save_activation_code(self, email, activation_code):
        """
        Save activation code, with a timestamp. Push it to a list, so we keep track of previous
        expected codes.
        """
        now = datetime.datetime.utcnow()
        self._users.update_one(
            {"email": email, "is_active": False},
            {"$push": {"activation_codes": (activation_code, now)}},
        )


    async def _set_user_active(self, email, value=True):
        # simply set is_active flag to True
        await self._users.update_one(
            {"email": email}, {"$set": {"is_active": value}}
        )

    async def activate_user(self, email, code_attempt):
        """
        Set user to active if:
        - not active yet
        - activation code is correct (same as last generated)
        - activation code is recent enough
        """

        user = await self._get_user_document(email)

        if user["is_active"]:
            return  # no need to go further

        try:
            code, code_timestamp = user["activation_codes"][-1]
        except IndexError:
            raise ActivationRequestNeeded()

        if code != code_attempt:
            raise IncorrectActivationCode()

        now = datetime.datetime.utcnow()
        if now.timestamp() - code_timestamp.timestamp() > MAX_ACTIVATION_DELAY:
            raise ActivationCodeExpired()

        await self._set_user_active(email)
