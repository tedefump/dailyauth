class UserAlreadyExists(Exception):
    pass


class UserNotFound(Exception):
    pass



class ActivationError(Exception):
    """ Base class for user activation related errors """
    pass


class ActivationRequestNeeded(ActivationError):
    pass


class ActivationCodeExpired(ActivationError):
    pass


class IncorrectActivationCode(ActivationError):
    pass
