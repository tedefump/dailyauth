""" A few utility functions to deal with passwords """

# NOTE: user password security is not my main focus.
# I'll use an approx. "good enough" way to store passwords.

import hashlib
import os
import uuid


def some_salt():
    return uuid.uuid4()


def some_pepper():
    # NOTE: This needs to stay stable
    return os.environ.get("DAILY_PEPPER", "no-pepper")


def get_hash(data):
    return hashlib.sha512(data).hexdigest().encode("utf-8")


def _hash_password(salt, pepper, password):
    hash_ = get_hash(f"{salt}.{password}.{pepper}".encode("utf-8"))
    for i in range(10):
        hash_ = get_hash(hash_)

    return hash_.decode("utf-8")


def get_hashed_password(password):
    salt, pepper = some_salt(), some_pepper()
    hashed = _hash_password(salt, pepper, password)
    return f"{salt}.{hashed}"


def check_password(password: str, hashed: str) -> bool:
    """ Compare a password with its hashed version """
    salt, hash_ = hashed.split(".")
    return hash_ == _hash_password(salt, some_pepper(), password)
