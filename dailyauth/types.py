from pydantic import BaseModel, EmailStr
from dailyauth import passwords as pwd


class NewPassword(str):
    @classmethod
    def long_enough(cls, s):
        if len(s) < 4:
            raise ValueError("Password must be at least 4 characters long")
        return s

    @classmethod
    def __get_validators__(cls):
        yield cls.long_enough
        ...  # a long list of password requirements
        yield pwd.get_hashed_password

    def __repr__(self):
        return f"NewPassword({super().__repr__()})"


class NewUserInfo(BaseModel):
    email: EmailStr
    password: NewPassword


class PublicUserDetails(BaseModel):
    email: EmailStr
    is_active: bool


class User(BaseModel):
    email: EmailStr
    is_active: bool
    hashed_password: str
