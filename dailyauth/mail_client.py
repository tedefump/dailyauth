"""
Small POC library to interface with the third party mail service
"""
import requests

# This could come from configuration, but I'm hardcoding url
# of our contenerized environement, for the demo.
SERVICE_API = "http://uber-smtp-provider:8000"

SENDER = "noreply@dailyauth.oo"


def send_email(receiver, subject, message):
    """ basic function to send an email through 3rd party """
    requests.post(
        f"{SERVICE_API}/email",
        json={
            "addr_from": SENDER,
            "addr_to": receiver,
            "subject": subject,
            "message": message,
        },
    )
