.PHONY: build test


build:
	docker-compose -f docker-compose.yml build

test: build
	docker-compose -f docker-compose.yml \
		run tests \
		poetry run pytest --cov=dailyauth

stop:
	docker-compose -f docker-compose.yml stop

up: build
	docker-compose -f docker-compose.yml up
