FROM python:3.8

WORKDIR /app

COPY pyproject.toml .
RUN pip install poetry

# First install to cache dependencies
RUN poetry install

COPY . ./
RUN poetry install
